enum CadinaIDirections {
    North = "North",
    East = "East",
    South = "South",
    West = "West"
}

let currentDirection = CadinaIDirections.East;

console.log(currentDirection);